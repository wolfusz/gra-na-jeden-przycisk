#include <LCD.h>
#include <Led.h>
#include <Button.h>

#include "game.h"

int freeRam() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

const int btnPin1 = 4;
const int btnPin2 = 5;
const int btnPin3 = 3;
const int RledPin = A1;
const int YledPin = A2;

void setup() {
  // initialize USB
  Serial.begin(9600);
  
  // initialize random number generator // works only if passed pin is unconnected
  randomSeed(analogRead(0));
  
  LCD::init(8, 9, 10, 12, 11, true);
  LCD::setContrast(60);

  Led::init(RledPin);
  Led::init(YledPin);
  
  Button::init(btnPin1);
  Button::init(btnPin2);
  Button::init(btnPin3);
  
  initGame();
}

void loop() {
  if (Button::getState(btnPin3) == release) {
    Serial.print(F("FREE RAM = "));
    Serial.println(freeRam());
  }
  
  gameControl(
    Button::getState(btnPin1)==release,
    Button::getState(btnPin2)==release
  );
  
  gameLoop();
  gameRender();
  
  Led::process(RledPin);
  Led::process(YledPin);
  delay(40);
}

