#include <Arduino.h>
#include <LCD.h>
#include <Led.h>
#include <Essentials.h>
#include "obstacle.h"
#include "player.h"

struct Obstacle {
	byte x,w,h;
	Obstacle* prev;
	Obstacle* next;
};

Obstacle* Obstacles::first = NULL;
Obstacle* Obstacles::last = NULL;

void Obstacles::init() {
	Obstacle* o = first;
	Obstacle* t;
	while (o != NULL) {
		t = o->next;
		destroy(o);
		o = t;
	}
}

bool Obstacles::spawn() {
    bool canspawn = true;
    if (last != NULL)
		if ((last->x + last->w) > 82)
			canspawn = false;
    if (canspawn) {
		byte width = random(7) + 1;
		byte height = random(Player::jumpHeight-2) + 1;
		
		Obstacle* o = (Obstacle*)malloc(sizeof(Obstacle));
		if (o == NULL)
			return false;
		
		o->w = width;
		o->h = height;
		o->x = 83;
		o->next = NULL;
		
		if (first == NULL) {
			o->prev = NULL;
			first = o;
		}
		o->prev = last;
		last = o;
		o->prev->next = o;
		
		Serial.print(F("OBSTACLE SPAWN x,w,h:"));
		Serial.print(i2s(o->x));
		Serial.print(F(","));
		Serial.print(i2s(o->w));
		Serial.print(F(","));
		Serial.println(i2s(o->h));
	}
	return canspawn;
}

void Obstacles::destroy(Obstacle* o) {
	Serial.print(F("OBSTACLE DESTROY x,w,h:"));
	Serial.print(i2s(o->x));
	Serial.print(F(","));
	Serial.print(i2s(o->w));
	Serial.print(F(","));
	Serial.println(i2s(o->h));
	
	if (o == first || o == last) {
		if (o == first) {
			first = o->next;
			first->prev = NULL;
		}
		if (o == last) {
			last = o->prev;
			last->next = NULL;
		}
	} else {
		if (o->prev != NULL)
			o->prev->next = o->next;
		if (o->next != NULL)
			o->next->prev = o->prev;
	}
	o->prev = NULL;
	o->next = NULL;
	free(o);
}

byte Obstacles::update(byte groundY) {
	// iterate through existing obstacles
	byte foundY = groundY;
	Obstacle* o = first;
	Obstacle* next;
	while (o != NULL) {
		// check if in player range
		if (o->x <= 3 && ((o->x + o->w) >= 3)) {
			foundY = groundY - o->h;
			// detect player collision
			if (Player::y > foundY)
				if (o->x == 3)
					Player::dead = true;
			if (o->x == 3) {
				Player::score++;
				Led::blink(A2, 5);
			}
		}
		
		next = o->next;
		
		// check if obstacle is visible
		if (o->x > 0)
			o->x -= 1;
		else {
			if (o->w > 0)
				o->w -= 1;
			else {
				destroy(o);
			}
		}
		
		// iterate through obstacles
		o = next;
	}
	
	// spawn new obstacle, 10% chance
	byte chance = random(100);
	if (chance < 10)
		spawn();
	
	return foundY;
}

void Obstacles::renderBG(byte groundY, byte groundW) {
	int startY, p;
	for (Obstacle* o = last; o != NULL; o = o->prev) {
		startY = groundY - o->h;
		
		// draw background edges
		LCD::line(o->x - groundW, startY - groundW, o->w, BLACK);
		LCD::line(o->x - groundW, startY - groundW, o->h, BLACK, true);
		
		for (p = 0; p < groundW; p++) {
			// clear box faces
			LCD::line(o->x - p, startY - p, o->w, WHITE);
			LCD::line(o->x - p, startY - p, o->h, WHITE, true);
			// draw box edges
			LCD::pixel(o->x - p       , startY - p , BLACK);
			LCD::pixel(o->x - p + o->w, startY - p , BLACK);
			LCD::pixel(o->x - p       , groundY - p, BLACK);
		}
	}
}

void Obstacles::renderFG(byte groundY, byte groundW) {
	int startY, p;
	for (Obstacle* o = last; o != NULL; o = o->prev) {
		startY = groundY - o->h;
		
		for (p = 1; p < groundW; p++) {
			// clear box faces
			LCD::line(o->x + p, startY + p, o->w, WHITE);
			LCD::line(o->x + p, startY + p, o->h, WHITE, true);
			// draw box edges
			LCD::pixel(o->x + p		  , startY + p , BLACK);
			LCD::pixel(o->x + p + o->w, startY + p , BLACK);
			LCD::pixel(o->x + p		  , groundY + p, BLACK);
		}
		
		// clear foreground face
		if (o->w > 1)
			LCD::rect(o->x + groundW + 1, startY + groundW + 1, o->w - 1, o->h, WHITE, true);
		// draw foreground edges
		LCD::line(o->x + groundW       , startY + groundW, o->w, BLACK);
		LCD::line(o->x + groundW       , startY + groundW, o->h, BLACK, true);
		LCD::line(o->x + groundW + o->w, startY + groundW, o->h, BLACK, true);
	}
}
