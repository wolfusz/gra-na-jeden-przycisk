#ifndef H_OBJECT
#define H_OBJECT

typedef struct Obstacle Obstacle;

class Obstacles {
public:
	static Obstacle* first;
	static Obstacle* last;
	static void init();
	static byte update(byte);
	static void renderBG(byte,byte);
	static void renderFG(byte,byte);
	static bool spawn();
	static void destroy(Obstacle*);
};

#endif