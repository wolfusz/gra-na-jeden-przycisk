#include <avr/pgmspace.h>
#include <Essentials.h>
#include "obstacle.h"
#include "player.h"

//## FLASH MEMORY STORAGE
const char str_menutitle[] PROGMEM = "BIEGATSCH v0.1";
const char str_endtitle[] PROGMEM = "YOU DIED!";
const char str_score[] PROGMEM = "SCORE:";
const char str_btnpress[] PROGMEM = "Press Button\n  to START";
const char str_credits[] PROGMEM = "Created by:\nBugi\2&\2Seba";
const char str_back[] PROGMEM = "Press BUTTON...";

const byte puchalke[] PROGMEM = {
	0x06, 0xf9, 0x01, 0x52, 0x54, 0x14, 0x12, 0x12, 0x12, 0x12, 0x52, 0x52, 0x12, 0x04, 0x02, 0xf1, 0x09, 0x06,
	0x00, 0x3f, 0x5c, 0xac, 0xa3, 0x20, 0x20, 0x20, 0x20, 0x10, 0x90, 0x90, 0xa0, 0x80, 0x40, 0x3f, 0x00, 0x00
};

//## GAME
enum GameState {
	menu,
	play,
	end
};
static GameState gameState;

void showScore(byte y) {
	byte x = LCD::string(0,y,loadString(str_score));
	LCD::string(x+2,y,i2s(Player::score));
}

const int groundLevel = 40;
const byte groundWidth = 3;

void initGame() {
	Obstacles::init();
	Player::init( groundLevel );
	
	gameState = menu;
	
	LCD::clear();
	LCD::rect(1,10,82,20);
	LCD::string(1,1,loadString(str_menutitle));
	LCD::string(16,13,loadString(str_btnpress));
	LCD::string(36,32,loadString(str_credits));
	LCD::update();
}

void endGame() {
	gameState = end;
	
	LCD::clear();
	
	LCD::string(0,0,loadString(str_endtitle));
	showScore(10);
	LCD::string(10,32,loadString(str_back));
	
	byte smuteg[36];
	memcpy_P(&smuteg, (byte*)(&puchalke), 36);
	LCD::bitmap(60,10,0,0,smuteg,NULL,BLACK,false,18,16);
	
	LCD::update();
}

void gameControl(bool menuBtn, bool jumpBtn) {
	switch (gameState) {
		case 0: {
			if (menuBtn == true)
				gameState = play;
		} break;
			
		case 1: {
			if (jumpBtn == true)
				Player::changeState(jumping);
			if (menuBtn == true)
				endGame();
		} break;
			
		case 2: {
			if (menuBtn == true)
				initGame();
		} break;
	}
}

void gameLoop() {
	if (gameState != play) return;
	
	byte foundY = Obstacles::update( groundLevel );
	if (Player::dead == true)
		endGame();
	else
		Player::update( foundY );
}


//## RENDERING
void gameRender() {
	if (gameState != play) return;
	
	LCD::clear();
	
	// BACKGROUND
	// render ground
	LCD::line(0, groundLevel-groundWidth, 84);
	// render obstacles
	Obstacles::renderBG(groundLevel, groundWidth);
	
	// PLAYER
	Player::render();
	
	// FOREGROUND
	// render ground
	LCD::line(0, groundLevel+groundWidth, 84);
	// render obstacles
	Obstacles::renderFG(groundLevel, groundWidth);
	
	showScore(0);
	LCD::update();
}
