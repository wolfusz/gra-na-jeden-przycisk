#include <avr/pgmspace.h>
#include <Arduino.h>
#include <LCD.h>
#include "player.h"

// initialize static variables
PlayerState Player::state = standing;
byte Player::animationFrame = 0;
bool Player::dead = true;
byte Player::y = 0;
byte Player::jumpHeight = 0;
byte Player::jumpStart = 0;
int Player::score = 0;
byte Player::currentAnimFrame[2][5] = {
	{0x90, 0xca, 0x3d, 0xca, 0x90},
	{0x00, 0x00, 0x00, 0x00, 0x00}
};
byte Player::currentAnimFrameAlpha[2][5] = {
	{0x90, 0xca, 0x3d, 0xca, 0x90},
	{0x00, 0x00, 0x00, 0x00, 0x00}
};

void Player::init(byte startY) {
	state = standing;
	animationFrame = false;
	
	dead = false;
	y = startY;
	jumpHeight = 8;
	jumpStart = 0;
	
	score = 0;
}


void Player::update(byte groundY) {
	bool onGround = (y >= groundY);
	
	if (onGround == true && y > groundY)
		y = groundY;
	
	switch (state) {
		case standing: {
			changeState(running);
		}
		case running: {
			if (onGround != true)
				changeState(falling);
		} break;
		
		case jumping: {
			if (y > jumpStart - jumpHeight) {
				y -= 2;
			} else {
				changeState(falling);
			}
		} break;
		case falling: {
			if (onGround != true) {
				y += 2;
			} else {
				changeState(standing);
				jumpStart = 0;
				y = groundY;
			}
		} break;
	}
}

const byte anim_idle[] PROGMEM = {0x90, 0xca, 0x3d, 0xca, 0x90};
const byte alpha_anim_idle[] PROGMEM = {0x90, 0xca, 0x3f, 0xca, 0x90};
const byte anim_jump[] PROGMEM = {0x00, 0x3e, 0xad, 0x52, 0x00};
const byte alpha_anim_jump[] PROGMEM = {0x00, 0x3e, 0xaf, 0x52, 0x00};
const byte anim_fall[] PROGMEM = {0x00, 0x02, 0x1d, 0xca, 0x00};
const byte alpha_anim_fall[] PROGMEM = {0x00, 0x02, 0x1f, 0xca, 0x00};
const byte anim_run1[] PROGMEM = {0x00, 0x02, 0xfd, 0x02, 0x00};
const byte alpha_anim_run1[] PROGMEM = {0x00, 0x02, 0xff, 0x02, 0x00};
const byte anim_run2[] PROGMEM = {0x00, 0x9a, 0x7d, 0x52, 0x80};
const byte alpha_anim_run2[] PROGMEM = {0x00, 0x9a, 0x7f, 0x52, 0x80};
const byte* const playerAnimations[] PROGMEM = { anim_idle, anim_jump, anim_fall, anim_run1, anim_run2 };
const byte* const playerAnimationsAlpha[] PROGMEM = { alpha_anim_idle, alpha_anim_jump, alpha_anim_fall, alpha_anim_run1, alpha_anim_run2 };

void Player::changeState(PlayerState newState) {
	// change state
	switch(newState) {
		// case standing: Serial.println(F("I STAND")); break;
		// case running: Serial.println(F("I RUN")); break;
		// case falling: Serial.println(F("I FALL")); break;
		case jumping: {
			if (state == jumping || state == falling)
				return;
			jumpStart = y;
			// Serial.println(F("I JUMP"));
		} break;
	}
	
	// load anims
	animationFrame = 0;
	int i;
	switch (state = newState) {
		// 1 frame anims
		case standing:
		case jumping:
		case falling: {
			memset(currentAnimFrame[1], 0, 5); // clear second frame
			memset(currentAnimFrameAlpha[1], 0, 5); // clear second frame alpha
			memcpy_P(&(currentAnimFrame[0]), (byte*)pgm_read_word(&(playerAnimations[state])), 5);
			memcpy_P(&(currentAnimFrameAlpha[0]), (byte*)pgm_read_word(&(playerAnimationsAlpha[state])), 5);
		} break;
		
		// 2 frames anim
		case running: {
			memcpy_P(&(currentAnimFrame[0]), (byte*)(&anim_run1), 5);
			memcpy_P(&(currentAnimFrameAlpha[0]), (byte*)(&alpha_anim_run1), 5);
			memcpy_P(&(currentAnimFrame[1]), (byte*)(&anim_run2), 5);
			memcpy_P(&(currentAnimFrameAlpha[1]), (byte*)(&alpha_anim_run2), 5);
		} break;
	}
}

void Player::render() {
	// get frame
	if (state == running)
		animationFrame = (animationFrame + 1) % 2;
	
	// draw bitmap
	LCD::bitmap(3, y, -2, -7, currentAnimFrame[animationFrame], currentAnimFrameAlpha[animationFrame], 1, false);
}
