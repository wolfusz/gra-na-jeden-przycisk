#ifndef H_PLAYER
#define H_PLAYER

enum PlayerState {
	standing,
	jumping,
	falling,
	running
};

class Player {
private:
	static PlayerState state;
	static byte animationFrame;
	static byte jumpStart;
	static byte currentAnimFrame[2][5];
	static byte currentAnimFrameAlpha[2][5];
	
public:
	static bool dead;
	static byte jumpHeight;
	static byte y;
	static int score;
	
	static void init(byte);
	static void update(byte);
	static void render();
	
	static void changeState(PlayerState);
};

#endif
