#ifndef H_OBSTACLE
#define H_OBSTACLE

byte obstacleMaxWidth = 7;

class Obstacle {
public:
	byte x,w,h;
	
	Obstacle() {}
	Obstacle(byte w, byte h) : w(w), h(h) { x = 83 - (w/2); }
};

vector<Obstacle> obstacles;

#endif
