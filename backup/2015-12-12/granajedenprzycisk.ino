#include <StandardCplusplus.h>
#include <system_configuration.h>
#include <unwind-cxx.h>
#include <utility.h>

#include <LCD.h>
#include <vector>
#include "obstacle.h"
#include "game.h"

void setup() {
  // initialize USB
  Serial.begin(9600);
  LCDinit(8, 9, 10, 12, 11);
  LCDsetContrast(60);
  initButton();
  initGame();
  // initialize random number generator // works only if passed pin is unconnected
  randomSeed(analogRead(0));
}

bool alreadyPressed;

void loop() {
  gameLoop();

  // do something after first button press
  if (buttonPressed() == true) {
    Serial.println("BUTTON PRESS");
    if (alreadyPressed == false) {
      alreadyPressed = true;
      gameControl();
    }
  } else alreadyPressed = false;

  // render everything
  LCDupdate();
}


