#include "player.h"
#include "obstacle.h"

//#define DEBUG

// convert integer to string
char* i2s(int i) {
	char* r = new char[6] {0x00};
	int w10 = 10, c, z = 0;
	while (i % w10 != i) w10 *= 10;
	while (w10 != 1) {
		w10 /= 10;
		c = ( i - (i % w10) ) / w10;
		r[z] = (char)(c+48);
		z++;
	}
	r[z] = '\0';
	return r;
}


int groundLevel = 40;
byte groundWidth = 3;


//## GAME
byte gameState; // 0 - start, 1 - playing, 2 - end

void initGame() {
	// and then init and start game
	Obstacle::init();
	Player::init(groundLevel);
	
	gameState = 0;
}

//## GAME LOGIC
void gameLoop() {
	if (gameState == 1) {
		// int foundY = updateObstacles();
		// Player::update( foundY );
		Player::update( groundLevel );
		delay(100);
	}
}


//## RENDERING
void gameRender() {
	switch (gameState) {
		case 0: {
			LCD::string(1,1,"BIEGATSCH v0.1",BLACK);
			LCD::rect(1,10,82,20,false,BLACK);
			LCD::string(16,13,"Press Button\n  to START",BLACK);
			LCD::string(36,32,"Created by:\nBugi\2&\3Seba",BLACK);
		} break;
		
		case 1: {
			LCD::clear(WHITE);
			LCD::string(0,0,"SCORE:",BLACK);
			LCD::string(55,0,i2s(Player::score),BLACK);
			
			// BACKGROUND
			// render ground
			LCD::line(1, groundLevel-groundWidth, 84, false, BLACK);
			// render obstacles
			Obstacle* o = Obstacle::last();
			while (o != NULL) {
				o->renderBG(groundLevel, groundWidth);
				o = o->prev;
			}
			
			// PLAYER
			Player::render();
			
			// FOREGROUND
			// render ground
			LCD::line(1, groundLevel+groundWidth, 84, false, BLACK);
			// render obstacles
			o = Obstacle::last();
			while (o != NULL) {
				o->renderFG(groundLevel, groundWidth);
				o = o->prev;
			}
		} break;
		
		case 2: {
			LCD::string(0,0,"YOU DIED!",BLACK);
			LCD::string(0,10,"YOUR SCORE:",BLACK);
			LCD::string(55,10,i2s(Player::score),BLACK);
		} break;
	}
}


//## CONTROLS
void gameControl() {
	switch (gameState) {
		case 0: {
			gameState = 1;
			Player::run();
		} break;
			
		case 1: {
			Player::jump();
		} break;
			
		case 2: {
			initGame();
		} break;
	}
}


// OBSTACLE RENDERING
// void renderObstaclesBackground() {
	// Obstacle* o = lastObstacle();
	// while (o != NULL) {
		// o->renderBackground();
		// o = o->prev;
	// }
// }

// void renderObstaclesForeground() {
	// Obstacle* o = lastObstacle();
	// while (o != NULL) {
		// o->renderForeground();
		// o = o->prev;
	// }
// }



//# GAME MECHANICS
// OBSTACLES LOGIC
// int updateObstacles() {
	// // iterate through existing obstacles
	// int foundY = groundLevel;
	// int startX;
	// Obstacle* o = firstObstacle;
	// while (o != NULL) {
		// // get obstacle's left edge
		// startX = o->x - (o->w/2);
		
		// // check if in player range
		// if (startX <= 3 && ((startX + o->w) >= 3)) {
			// foundY = groundLevel - o->h;
			// // detect player collision
			// if (playerY > foundY)
				// gameState = 2;
			// else if (startX == 3)
				// score++;
		// }
		
		// Obstacle* next = o->next;
		
		// if (startX + o->w > 0) // obstacle is visible
			// o->x -= 1;
		// else // delete obstacle
			// deleteObstacle(o);
		
		// // iterate through obstacles
		// o = next;
	// }
	
	// // spawn new obstacle, 10% chance
	// byte chance = random(100);
	// if (chance < 10) {
		// Obstacle* last = lastObstacle();
		// if (last != NULL) {
			// byte width = random(obstacleMaxWidth) + 1;
			// if ( last->x < LCD::WIDTH-width) {
				// byte height = random(playerJumpHeight-2) + 1;
				// spawnObstacle(width,height);
			// }
		// }
	// }
	
	// return foundY;
// }
