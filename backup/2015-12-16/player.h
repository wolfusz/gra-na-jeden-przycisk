#ifndef H_PLAYER
#define H_PLAYER

class Player {
private:
	static byte state;
	static byte animationFrame;
	static byte y;
	static byte jumpHeight;
	static byte jumpStart;
	
public:
	static int score;
	
	static byte State() { return state; }
	static byte Y() { return y; }
	
	static void init(byte);
	static void update(byte);
	static void render();
	
	static void run();
	static void jump();
};

#endif
