#include <Arduino.h>
#include <LCD.h>
#include "obstacle.h"

Obstacle* Obstacle::first = NULL;

Obstacle* Obstacle::last() {
	Obstacle* o = Obstacle::first;
	while (o->next != NULL)
		o = o->next;
	return o;
}

void Obstacle::init() {
	Obstacle* o = Obstacle::first;
	Obstacle* t;
	while (o != NULL) {
		t = o->next;
		delete o;
		o = t;
	}
}

Obstacle::Obstacle(byte w, byte h) {
	this->w = w;
	this->h = h;
	
	this->x = 83+w/2;
	this->next = NULL;
	
	if (Obstacle::first == NULL) {
		Obstacle::first = this;
		this->prev = NULL;
	} else {
		Obstacle* o = Obstacle::first;
		while (o->next != NULL)
			o = o->next;
		o->next = this;
		this->prev = o;
	}
}

Obstacle::~Obstacle() {
	if (this->prev != NULL)
		this->prev->next = this->next;
	if (this->next != NULL)
		this->next->prev = this->prev;
}

void Obstacle::renderBG(byte groundY, byte groundW) {
	byte startX = this->x - (this->w/2);
	byte startY = groundY - this->h;
		
	// draw background edges
	LCD::line(startX - groundW, startY - groundW, this->w, false, 1);
	LCD::line(startX - groundW, startY - groundW, this->h, true, 1);
		
	for (byte p = 0; p < groundW; p++) {
		// clear box faces
		LCD::line(startX - p, startY - p, this->w, false, 1);
		LCD::line(startX - p, startY - p, this->h, true, 1);
		// draw box edges
		LCD::pixel(startX - p       , startY - p, 1);
		LCD::pixel(startX - p + this->w, startY - p, 1);
		LCD::pixel(startX - p       , groundY - p, 1);
	}
}

void Obstacle::renderFG(byte groundY, byte groundW) {
	byte startX = this->x - (this->w/2);
	byte startY = groundY - this->h;
		
	for (byte p = 1; p < groundW; p++) {
		// clear box faces
		LCD::line(startX + p, startY + p, this->w, false, 1);
		LCD::line(startX + p, startY + p, this->h, true, 1);
		// draw box edges
		LCD::pixel(startX + p       , startY + p, 1);
		LCD::pixel(startX + p + this->w, startY + p, 1);
		LCD::pixel(startX + p       , groundY + p, 1);
	}
		
	// draw foreground edges
	LCD::rect(startX+1, startY+1, this->w-2, this->h-2, true, 0);
	LCD::line(startX + groundW      , groundY - this->h + groundW, this->w, false, 1);
	LCD::line(startX + groundW      , groundY - this->h + groundW, this->h, true, 1);
	LCD::line(startX + groundW + this->w, groundY - this->h + groundW, this->h, true, 1);
}

bool Obstacle::update() {
	return false;
}