#include <LCD.h>
#include <Button.h>

#include "game.h"

void setup() {
  // initialize USB
  Serial.begin(9600);
  LCD::init(8, 9, 10, 12, 11, true);
  LCD::setContrast(60);
  Button::init(7);
  initGame();
  // initialize random number generator // works only if passed pin is unconnected
  randomSeed(analogRead(0));
}

void loop() {
  gameLoop();

  // do something after first button press
  if (Button::getState(7) == release) {
    Serial.println("BUTTON PRESS");
    gameControl();
  }

  // render everything
  gameRender();
  LCD::update();
}

