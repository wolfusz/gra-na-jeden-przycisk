#ifndef H_OBJECT
#define H_OBJECT

class Obstacle {
public:
	static Obstacle* first;
	static Obstacle* last();
	static void init();
	
	byte x,w,h;
	Obstacle* prev;
	Obstacle* next;
	
	Obstacle(byte=1,byte=1);
	~Obstacle();
	
	bool update();
	
	void renderBG(byte,byte);
	void renderFG(byte,byte);
};

#endif